package com.example.danco.homework5.h255danco.data;

import android.content.ContentValues;

import java.util.Calendar;

public class DataUtilities {

    /* package */
    static ContentValues createContactValues() {
        ContentValues values = new ContentValues();
        values.put(ContactContract.Columns.NAME, "Dan Costinett");
        values.put(ContactContract.Columns.ADDRESS, "925 4th Ave S., \nSeattle, WA 98104");
        Calendar date = Calendar.getInstance();
        date.set(1967, Calendar.AUGUST, 6, 0, 0, 0);
        values.put(ContactContract.Columns.BIRTH_DATE, date.getTimeInMillis());
        return values;
    }

    /* package */
    static ContentValues updateContactValues() {
        ContentValues values = new ContentValues();
        values.put(ContactContract.Columns.NAME, "Dylan Costinett");
        values.put(ContactContract.Columns.ADDRESS, "925 4th Ave S., \nSeattle, WA 98777");
        Calendar date = Calendar.getInstance();
        date.set(2001, Calendar.MAY, 20, 0, 0, 0);
        values.put(ContactContract.Columns.BIRTH_DATE, date.getTimeInMillis());
        return values;
    }

}
