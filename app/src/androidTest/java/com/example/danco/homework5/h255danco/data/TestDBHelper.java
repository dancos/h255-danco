package com.example.danco.homework5.h255danco.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Basic test class to test the database.
 */
public class TestDBHelper extends AndroidTestCase {


    // Since we want each test to start with a clean slate
    void deleteTheDatabase() {
        getContext().deleteDatabase(DBHelper.DATABASE_NAME);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        deleteTheDatabase();
    }


    public void testBuildDb() throws Throwable {

        File file = getContext().getDatabasePath(DBHelper.DATABASE_NAME);
        assertFalse("Database already exists", file.exists());

        final HashSet<String> tableNameHashSet = new HashSet<>();
        tableNameHashSet.add(ContactContract.TABLE);

        DBHelper helper = new DBHelper(getContext());
        SQLiteDatabase db = helper.getWritableDatabase();
        assertEquals(true, db.isOpen());

        // have we created the tables we want?
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

        assertTrue("Error: This means that the database has not been created correctly",
                c.moveToFirst());

        // verify that the tables have been created
        do {
            tableNameHashSet.remove(c.getString(0));
        } while (c.moveToNext());

        // if this fails, it means that your database doesn't contain both tables
        assertTrue("Error: Your database was created without correct tables",
                tableNameHashSet.isEmpty());

        // now, do our tables contain the correct columns?
        c = db.rawQuery("PRAGMA table_info(" + ContactContract.TABLE + ")", null);

        assertTrue("Error: This means that we were unable to query the database for table information.",
                c.moveToFirst());

        // Build a HashSet of all of the column names we want to look for
        final HashSet<String> stateColumnHashSet = new HashSet<>();
        stateColumnHashSet.add(ContactContract.Columns._ID);
        stateColumnHashSet.add(ContactContract.Columns.NAME);
        stateColumnHashSet.add(ContactContract.Columns.ADDRESS);
        stateColumnHashSet.add(ContactContract.Columns.BIRTH_DATE);

        int columnNameIndex = c.getColumnIndex("name");
        do {
            String columnName = c.getString(columnNameIndex);
            stateColumnHashSet.remove(columnName);
        } while (c.moveToNext());

        c.close();

        // if this fails, it means that your database doesn't contain all of the required state
        // entry columns
        assertTrue("Error: The database doesn't contain all of the required state columns",
                stateColumnHashSet.isEmpty());

        db.close();
        helper.close();
    }


    public void testCRUDContactTable() throws Throwable {
        // CRUD = Create, Read, Update, Delete

        // First step: Get reference to writable database
        // If there's an error in those massive SQL table creation Strings,
        // errors will be thrown here when you try to get a writable database.
        DBHelper dbHelper = new DBHelper(getContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // Second Step: Create ContentValues of what you want to insert
        ContentValues insertValues = DataUtilities.createContactValues();

        // Third Step: "Create" Insert ContentValues into database and get a row ID back
        long rowId;
        rowId = db.insert(ContactContract.TABLE, null, insertValues);

        // Fourth Step: "Read" Query the database and receive a Cursor back
        // A cursor is your primary interface to the query results.
        Cursor insertCursor = db.query(
                ContactContract.TABLE,  // Table to Query
                null, // all columns
                null, // Columns for the "where" clause
                null, // Values for the "where" clause
                null, // columns to group by
                null, // columns to filter by row groups
                null // sort order
        );

        // Fifth Step: Validate the Query. Adding id values for validation purposes
        insertValues.put(ContactContract.Columns._ID, rowId);
        validateCursor("Contact Cursor", insertCursor, insertValues);
        insertCursor.close();

        // Sixth Step: Create content values for "update"
        ContentValues updateValues = DataUtilities.updateContactValues();

        // Seventh Step: Update table
        final String[] whereArgs = { Long.toString(rowId) };
        int rows = db.update(ContactContract.TABLE, updateValues, "_id = ?", whereArgs);
        assertEquals("Unexpected number of rows updated", rows, 1);

        // Eighth Step: "Read" Query the database and receive a Cursor back
        // A cursor is your primary interface to the query results.
        Cursor updateCursor = db.query(
                ContactContract.TABLE,  // Table to Query
                null, // all columns
                null, // Columns for the "where" clause
                null, // Values for the "where" clause
                null, // columns to group by
                null, // columns to filter by row groups
                null // sort order
        );

        // Ninth Step: Validate the Query. Adding id values for validation purposes
        insertValues.put(ContactContract.Columns._ID, rowId);
        validateCursor("Contact Cursor", updateCursor, updateValues);
        updateCursor.close();

        // Tenth Step: Delete data
        int countDeletedRows = db.delete(ContactContract.TABLE, "_id = ?", whereArgs);
        assertEquals("More than one row", countDeletedRows, 1);

        // Eleventh Step: "Read" Query the database and receive a Cursor back
        // A cursor is your primary interface to the query results.
        Cursor deleteCursor = db.query(
                ContactContract.TABLE,  // Table to Query
                null, // all columns
                null, // Columns for the "where" clause
                null, // Values for the "where" clause
                null, // columns to group by
                null, // columns to filter by row groups
                null // sort order
        );

        assertFalse("NonEmpty cursor returned. ", deleteCursor.moveToFirst());
        deleteCursor.close();

        // Last Step: cleanup
        db.close();
        dbHelper.close();
    }


    private static void validateCursor(String error, Cursor valueCursor, ContentValues expectedValues) {
        assertTrue("Null cursor returned." + error, valueCursor != null);
        assertTrue("Empty cursor returned. " + error, valueCursor.moveToFirst());
        validateCurrentRecord(error, valueCursor, expectedValues);

        // Move the cursor to demonstrate that there is only one record in the database
        assertFalse("Error: More than one record returned in cursor. " + error, valueCursor.moveToNext());
    }


    private static void validateCurrentRecord(String error, Cursor valueCursor, ContentValues expectedValues) {
        Set<Map.Entry<String, Object>> valueSet = expectedValues.valueSet();
        for (Map.Entry<String, Object> entry : valueSet) {
            String columnName = entry.getKey();
            int idx = valueCursor.getColumnIndex(columnName);
            assertFalse("Column '" + columnName + "' not found. " + error, idx == -1);
            String expectedValue = entry.getValue().toString();
            assertEquals("Value '" + entry.getValue().toString() +
                    "' did not match the expected value '" +
                    expectedValue + "'. " + error, expectedValue, valueCursor.getString(idx));
        }
    }

}
