package com.example.danco.homework5.h255danco.data;

/**
 * Created by costd035 on 2/20/15.
 */
/* package */ interface BaseContract {
    // The authority should always be your package name. This eliminates conflicts
    // with other applications if you ever decide to "export" your provider
    ///* package */ static final String AUTHORITY = "com.example.danco.homework5.h255danco.data";

    // The base URI for all other URIs. Starts with "content" to indicate it will come
    // from a content provider
    ///* package */ static final String BASE_URI = "content://" + AUTHORITY + "/";

}