package com.example.danco.homework5.h255danco.data;

import android.provider.BaseColumns;

/**
 * Created by costd035 on 2/20/15.
 */
public interface ContactContract {

    public interface Columns extends BaseColumns {
        // _ID provided by base columns
        public String NAME = "name";
        public String ADDRESS = "address";
        public String BIRTH_DATE = "birth_date";
    }

    /* package */ static final String TABLE = "contact";

    // Provider URI
    ///* package */ static final String URI_STRING = BaseContract.BASE_URI + TABLE;
    // public static final Uri URI = Uri.parse(URI_STRING);

    // Database statements
    /* package */ static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE + " ( " +
                    BaseColumns._ID + " INTEGER PRIMARY KEY, " +
                    Columns.NAME + " TEXT NOT NULL, " +
                    Columns.ADDRESS + " TEXT NOT NULL, " +
                    Columns.BIRTH_DATE + " INTEGER NOT NULL " +
                    " )";
}
