package com.example.danco.homework5.h255danco.data;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

/**
 * Created by costd035 on 2/20/15.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    /* package */ static final String DATABASE_NAME = "SampleContacts.db";


    // If you are not using a ContentProvider to access your database, you
    // should use a singleton to because database is thread safe. The
    // singleton for the helper allows the app to have better throughput
    // especially if there are multiple tables or independent reads/writes
    //
    // private static DBHelper helper;
    //
    // public synchronized static DBHelper getInstance(Context context) {
    //    if (helper == null) {
    //        helper = new DBHelper(context);
    //   }
    //    return helper;
    // }

    // If you where using a singleton, this constructor should be made private
    // However, planning to use in a ContentProvider next week so it is public
    public DBHelper(Context context) {
        // Using application context since if in a singleton, the helper likely will
        // outlive the activity, asyncTask etc that starts it.

        // the "null" indicates we want to use the default cursor factory
        super(context.getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);


        // Write ahead logging allows multiple threads access the database
        // at same time which improves performance. Essentially a writer will no
        // longer block a reader accessing the database at the same time.
        // This method configures it for the active or next instance.
        // It can also be used to toggle it off.
        //
        // Note: if you use transactions, you need to run the transactions in
        // non-exclusive mode see
        // http://developer.android.com/reference/android/database/sqlite/SQLiteDatabase.html#enableWriteAheadLogging()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setWriteAheadLoggingEnabled(true);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);

        // By default foreign key constraints are NOT enforced. This
        // was a surprise when I first encountered. Therefore you should turn
        // them on if you actually want them to help you debug schema problems.
        //
        // Note: if you do add a foreign key, you should also create an index for
        // each one as it should help with performance.
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);

        // If you are supporting OS versions before JellyBean, the onConfigure and write
        // ahead logging methods do not exist.  This is how you activate
        // for older releases.  You can only do this on a writable database.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN && !db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=ON;");
            db.enableWriteAheadLogging();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // This is where you create the tables for the database. Should create in
        // dependency order. Since city has a FK to the state table, create state first.
        db.execSQL(ContactContract.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // This is test code that completely rebuilds the database if the version changes.
        // If this is a temporary "cache", then its ok to completely delete and rebuild
        // as versions change. Although if it is a "cache" you may need to "delete" stale
        // data periodically between releases.
        cleanupDatabase(db);
        onCreate(db);

        // If you need to preserve data, you should create upgrade sql statements for
        // each object you are modifying. In most cases this will be an ALTER statement
        // of some type. However, you may be CREATING/DROPPING objects as well. Each
        // object should be a separate SQL statement vs trying to build one statement for all.
        //
        // In this example, lets assume we made some changes to both tables
        // when we released version 2.  For version 3, we only updated the city table.
        //
        // if (oldVersion < 2) {  // updates to version 2
        //       db.execSQL(StateContract.UPGRADE_TABLE_v2);
        //       db.execSQL(CityContract.UPGRADE_TABLE_v2);
        // }
        //
        // if (oldVersion < 3) { // updates to version 3
        //      db.execSQL(CityContract.UPGRADE_TABLE_v3);
        // }
    }

    private static void cleanupDatabase(SQLiteDatabase db) {
        // Using if exists to prevent errors if the index or table does not exist
        db.execSQL("DROP TABLE IF EXISTS " + ContactContract.TABLE);
    }

}
